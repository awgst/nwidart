<?php

namespace App\Http\Controllers;

use Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class StoredFileController extends Controller
{
    //
    public function read($filename){
        // $path = storage_path('app/company/' . $filename);
        $fileExist = Storage::exists('company/'.$filename);
        if (!$fileExist) {
            abort(404);
        }
        else{
            $file = Storage::get('company/'.$filename);
            $type = Storage::mimeType('company/'.$filename);
            
            $response = Response::make($file, 200);
            $response->header("Content-Type", $type);
        
            return $response;
        }
    
    }
}
