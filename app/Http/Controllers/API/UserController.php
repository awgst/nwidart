<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController;
use App\Http\Requests\UserLoginRequest;
use Illuminate\Contracts\Validation\Validator as ValidationValidator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserController extends BaseController
{
    //
    public function login(Request $request){
        $validate = Validator::make($request->all(), ['email'=>'required', 'password'=>'required']);
        if($validate->fails()){
            return $this->responseError('Login failed', 422, $validate->errors());
        }
        if (Auth::attempt($request->all())) {
            $user = Auth::user();
            $response = [
                'token'=>$user->createToken('UserToken')->accessToken,
                'name'=>$user->name,
            ];
            return $this->responseOk($response);
        }
        else {
            return $this->responseError('Login failed', 401);
        }
    }

    public function profile(Request $request){
        return $this->responseOk($request->user());
    }
}
