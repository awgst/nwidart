@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="d-flex flex-row-reverse my-3">
                <a href="{{ route('company.print', $company->id) }}" class="btn btn-success">Export PDF</a>
            </div>
            <div class="card">
                <div class="card-header">Edit {{ $company->name }}</div>
                <input type="hidden" value="{{ $company->id }}" id="company_id">
                <div class="card-body">
                    @include('transisi::snippets.company.employeepagination')
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        let company_id = $('#company_id').val();
        $(document).on('click', '.page-link', function (e) {
            e.preventDefault();
            let page = $(this).attr('href').split('page=')[1];
            fetch(page);
        });

        function fetch(page){
            $.ajax({
                type: "GET",
                url: "/transisi/company/"+company_id+"?page="+page,
                dataType: "json",
                success: function (response) {
                },
                error:function(xhr){
                    if(xhr.status==200){
                        $('.card-body').html(xhr.responseText);
                    }
                }
            });
        }
    });
</script>
@endsection
