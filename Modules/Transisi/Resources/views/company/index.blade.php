@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if (session('msg'))
                <div class="alert {{ strpos(session('msg'),'gagal') ? 'alert-danger':'alert-success' }}">{{ session('msg') }}</div>
            @endif
            <div class="d-flex flex-row-reverse mb-3">
                <a href="{{ route('company.create') }}" class="btn btn-primary">Tambah</a>
                <a href="{{ route('company.export') }}" class="btn btn-outline-primary mx-2">Export</a>
            </div>
            <div class="card">
                <div class="card-header">{{ __('Company') }}</div>
                <div class="card-body">
                    @include('transisi::snippets.company.pagination')
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $(document).on('click', '.page-link', function (e) {
            e.preventDefault();
            let page = $(this).attr('href').split('page=')[1];
            fetch(page);
        });

        function fetch(page){
            $.ajax({
                type: "GET",
                url: "/transisi/company?page="+page,
                data: {paginate:'true'},
                dataType: "json",
                success: function (response) {
                },
                error:function(xhr){
                    if(xhr.status==200){
                        $('.card-body').html(xhr.responseText);
                    }
                }
            });
        }
    });
</script>
@endsection
