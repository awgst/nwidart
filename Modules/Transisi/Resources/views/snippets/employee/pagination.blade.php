<div class="list-group mb-2">
    @foreach($employees as $employee)
    <div class="list-group-item list-group-item-action mb-2">
          <div class="d-flex w-100 justify-content-between">
              <a href="{{ route('employee.show', $employee->id) }}" class="text-decoration-none text-dark"><h5 class="mb-1">{{ $employee->name }}</h5></a>
            <small class="d-flex">
              <a href="{{ route('employee.edit', $employee->id) }}" class="btn btn-success mx-1">Edit</a>
              <form action="{{ route('employee.destroy', $employee->id) }}" method="POST">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger mx-1">Hapus</button>
              </form>
            </small>
          </div>
          <p class="mb-1">{{ $employee->email }}</p>
          <small>Bekerja di {{ $employee->company->name }}</small>    
    </div>
    @endforeach
</div>
<div class="d-flex justify-content-end">
    {{ $employees->links() }}    
</div>