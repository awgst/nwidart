<div class="list-group mb-2">
    @foreach($companies as $company)
        <div class="list-group-item list-group-item-action mb-2">
            <div class="d-flex">
                <div class="logo" style="width: 100px;height: 100px">
                    <img class="w-100 h-100" src="{{ url($company->logo) }}"></img>
                </div>
                <div class="description w-100 mx-3">
                    <div class="d-flex w-100 justify-content-between">
                    <a href="{{ route('company.show', $company->id) }}" class="text-decoration-none text-dark"><h5 class="mb-1">{{ $company->name }}</h5></a>
                    <small class="d-flex">
                        <a href="{{ route('company.edit', $company->id) }}" class="btn btn-success mx-1">Edit</a>
                        <form action="{{ route('company.destroy', $company->id)  }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger mx-1">Hapus</button>
                        </form>
                    </small>
                    </div>
                    <h5 class="mb-1"></h5>
                    <p class="mb-1">{{ $company->email }}</p>
                    <small class="text-muted">{{ $company->website }}</small>        
                </div>
            </div>
        </div>
    @endforeach
</div>
<div class="d-flex justify-content-end">
    {{ $companies->links() }}    
</div>