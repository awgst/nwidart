@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Karyawan</div>

                <div class="card-body">
                    <form action="{{ route('employee.update', $employee->id) }}" method="POST">
                      @csrf
                      @method('PUT')
                      <input type="hidden" name="employee_company" value="{{ $employee->company->id }}">
                      <div class="mb-3">
                        <select name="company_id" class="form-select" aria-label="Default select example" id="inputCompany">
                          <option selected disabled>Perusahaan</option>
                        </select>
                        @error('company_id')
                          <div class="text-danger">
                              {{ $message }}
                          </div>          
                        @enderror
                      </div>
                      <div class="mb-3">
                        <select name="status" class="form-select" aria-label="Default select example" id="inputStatus" required>
                          <option selected disabled>Status</option>
                          @foreach (Status::labels() as $key => $value)
                              <option {{ ($key===$employee->status)?'selected':'' }} value="{{ $key }}">{{ $value }}</option>
                          @endforeach
                        </select>
                        @error('status')
                          <div class="text-danger">
                              {{ $message }}
                          </div>          
                        @enderror
                      </div>
                      <div class="mb-3">
                        <label for="inputName" class="form-label">Nama Karyawan</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" placeholder="Nama" id="inputName" name="name" required value="{{ $employee->name }}">
                        @error('name')
                          <div class="text-danger">
                              {{ $message }}
                          </div>          
                        @enderror
                      </div>
                      <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Alamat Email</label>
                        <input type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email" id="exampleInputEmail1" name="email" required value="{{ $employee->email }}">
                        @error('email')
                          <div class="text-danger">
                              {{ $message }}
                          </div>         
                        @enderror
                      </div>
                      <button type="submit" class="btn btn-primary">Edit</button>
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<script>
  $(document).ready(function () {
    let company_id = $("input[name='employee_company']").val();
    $.ajax({
      type: "GET",
      url: "/transisi/company",
      dataType: "json",
      success: function (response) {
          $.each(response.companies, function (index, value) { 
            let selected = (value.id==company_id) ? 'selected' : '';
            $('#inputCompany').append(`<option value="`+value.id+`" `+selected+`>`+value.name+`</option>`);
          });
      }
    });
  });
</script>
@endsection
