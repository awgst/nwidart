@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
          <div class="d-flex flex-row-reverse mb-3">
              <form action="{{ route('employee.import') }}" method="POST" enctype="multipart/form-data" >
                  @csrf
                  <input type="file" name="csv" accept=".csv" required>
                  <button class="btn btn-primary">Import</button>
              </form>
          </div>
            <div class="card">
                <div class="card-header">Tambah Karyawan</div>
                <div class="card-body">
                    <form action="{{ route('employee.store') }}" method="POST">
                      @csrf
                      <div class="mb-3">
                        <select name="company_id" class="form-select" aria-label="Default select example" id="inputCompany">
                          <option selected disabled>Perusahaan</option>
                        </select>
                        @error('company_id')
                          <div class="text-danger">
                              {{ $message }}
                          </div>          
                        @enderror
                      </div>
                      <div class="mb-3">
                        <label for="inputName" class="form-label">Nama Karyawan</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" placeholder="Nama" id="inputName" name="name" required value="{{ old('name') }}">
                        @error('name')
                          <div class="text-danger">
                              {{ $message }}
                          </div>          
                        @enderror
                      </div>
                      <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Alamat Email</label>
                        <input type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email" id="exampleInputEmail1" name="email" required value="{{ old('email') }}">
                        @error('email')
                          <div class="text-danger">
                              {{ $message }}
                          </div>         
                        @enderror
                      </div>
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<script>
  $(document).ready(function () {
    $.ajax({
      type: "GET",
      url: "/transisi/company",
      dataType: "json",
      success: function (response) {
          $.each(response.companies, function (index, value) { 
            $('#inputCompany').append(`<option value="`+value.id+`">`+value.name+`</option>`);
          });
      }
    });
  });
</script>
@endsection
