<?php
namespace Modules\Transisi\Repositories;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Modules\Transisi\Entities\Employee;

class EmployeeRepository
{    
    public function __construct(Employee $employee)
    {
        $this->employee = $employee;
    }

    public function all()
    {
        return $this->employee->get();
    }

    public function find($id)
    {
        return $this->employee->find($id);
    }

    public function paginate($number)
    {
        return $this->employee->with('company')->where('status', 1)->paginate($number);
    }

    public function store(array $request)
    {
        return $this->employee->insert([
            'company_id' => $request['company_id'],
            'name' => $request['name'],
            'email' => $request['email'],
            'status' => 1
        ]);
    }

    public function update(array $request, int $id)
    {
        $update = $this->employee->where('id', $id)
                                 ->update([
                                     'company_id' => $request['company_id'],
                                     'name'=>$request['name'],
                                     'email' => $request['email'],
                                     'status' => $request['status']
                                 ]);
        return $update;
    }

    public function destroy(int $id)
    {
        return $this->employee->find($id)->delete();
    }

    public function export()
    {
        $employees = DB::table('employees')
            ->select('employees.id', 'employees.name', 'employees.email', 'employees.company_id', 'employees.status')
            ->orderBy('id');
        foreach ($employees->cursor() as $employee) {
            yield $employee;
        }
    }

    public function import($list)
    {
        DB::table('employees')->insert($list);
    }

}