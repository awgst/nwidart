<?php
namespace Modules\Transisi\Repositories;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Validator;
use Modules\Transisi\Entities\Company;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class CompanyRepository
{    
    public function __construct(Company $company)
    {
        $this->company = $company;
    }

    public function all()
    {
        return $this->company->get();
    }

    public function find($id)
    {
        return $this->company->find($id);
    }

    public function paginate($number)
    {
        return $this->company->paginate($number);
    }

    public function store(array $request)
    {
        return $this->company->insert([
            'name' => $request['name'],
            'email' => $request['email'],
            'logo' => $request['logo'],
            'website' => $request['website']
        ]);
    }

    public function update(array $request, int $id)
    {
        $company = $this->company->find($id);
        $update = $this->company->where('id', $id)
                                ->update([
                                    'name'=>$request['name'],
                                    'email' => $request['email'],
                                    'logo' => $request['logo'],
                                    'website' => $request['website']
                                ]);
        return $update;
    }

    public function destroy(int $id)
    {
        $company = $this->company->find($id);
        // Delete all employees from company
        $company->employees()->delete();
        $company->delete();
        return $company;
    }

    public function print($id)
    {
        $company = $this->company->find($id);
        $employees = DB::table('employees')
                        ->select('employees.*')
                        ->join('companies', 'companies.id', '=', 'employees.company_id')
                        ->where('employees.company_id', $id)
                        ->orderBy('id')
                        ->paginate(5);
        return [
            'company'=>$company,
            'employees'=>$employees
        ];
    }

    public function export()
    {
        $companies = DB::table('companies')
            ->select('companies.id', 'companies.name', 'companies.email', 'companies.website')
            ->orderBy('id');
        foreach ($companies->cursor() as $company) {
            yield $company;
        }
    }

    public function import($list)
    {
        DB::table('companies')->insert($list);
    }
}