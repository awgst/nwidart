<?php

namespace Modules\Transisi\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CompanyUpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name'=>'required',
            'email'=>['required', Rule::unique('companies')->ignore($this->segment(3))],
            'website'=>'required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the failed validation errors.
     *
     * @return response
     */
    protected function failedValidation(Validator $validator) {
        if(strpos($this->getRequestUri(), 'api')){
            throw new HttpResponseException(response()->json($validator->errors(), 422));
        }
    }
}
