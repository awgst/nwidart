<?php

namespace Modules\Transisi\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class EmployeeUpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'company_id'=>'required',
            'name'=>'required',
            'email'=>['required', Rule::unique('employees')->ignore($this->segment(3))],
            'status'=>'required'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the failed validation errors.
     *
     * @return response
     */
    protected function failedValidation(Validator $validator) {
        if(strpos($this->getRequestUri(), 'api')){
            throw new HttpResponseException(response()->json($validator->errors(), 422));
        }
    }
}
