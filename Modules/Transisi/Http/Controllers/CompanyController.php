<?php

namespace Modules\Transisi\Http\Controllers;

use Exception;
use PDF;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\LazyCollection;
use Modules\Transisi\Http\Requests\CompanyStoreRequest;
use Modules\Transisi\Http\Requests\CompanyUpdateRequest;
use Modules\Transisi\Repositories\CompanyRepository;
use Rap2hpoutre\FastExcel\FastExcel;

class CompanyController extends Controller
{
    function __construct(CompanyRepository $companyRepository)
    {
        $this->companyRepository = $companyRepository;
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            if($request->paginate=='true'){
                return view('transisi::snippets.company.pagination', ['companies'=>$this->companyRepository->paginate(5)])->render();
            }
            return response()->json(['msg'=>'Success', 'companies'=>$this->companyRepository->all()]);
        }
        return view('transisi::company.index', ['companies'=>$this->companyRepository->paginate(5)]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('transisi::company.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(CompanyStoreRequest $request)
    {
        //
        $validated = $request->validated();
        // Menyimpan gambar
        $fileName = date("Ymd").time().$request->logo->getClientOriginalName();
        $pathFile = $request->logo->storeAs('company', $fileName);
        $logo = 'storage/app/'.$pathFile;
        $validated['logo'] = $logo;
        // Menyimpan data kedalam database
        try {
            $this->companyRepository->store($validated);
            $msg = 'Data berhasil ditambahkan!';
        } catch (Exception $e) {
            $msg = 'Data gagal ditambahkan!';
        }
        return redirect('transisi/company')->with('msg', $msg);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        $company = $this->companyRepository->find($id);
        $employees = $company->employees()->paginate(5);
        if(request()->ajax()){
            return view('transisi::snippets.company.employeepagination', ['company'=>$company, 'employees'=>$employees])->render();
        }
        return view('transisi::company.show', ['company'=>$company, 'employees'=>$employees]);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $company = $this->companyRepository->find($id);
        return view('transisi::company.edit', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(CompanyUpdateRequest $request, $id)
    {       
        $company = $this->companyRepository->find($id) ;
        $validated = $request->validated();
        // Menyimpan gambar
        if(isset($request->logo)){
            $fileName = date("Ymd").time().$request->logo->getClientOriginalName();
            $pathFile = $request->logo->storeAs('company', $fileName);
            $logo = 'storage/app/'.$pathFile;
            $validated['logo'] = $logo;
            // Delete old image when user update the image
            Storage::delete(str_replace('storage/app/', '', $company->logo));
        } else{
            $validated['logo'] = $company->logo;
        }
        // Update data
        try {
            $this->companyRepository->update($validated, $id);
            $msg = 'Data berhasil diubah!';
        } catch (Exception $e) {
            $msg = 'Data gagal diubah!';
        }
        return redirect('transisi/company')->with('msg', $msg);

    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
        $company = $this->companyRepository->find($id);
        // Delete logo saved on storage
        Storage::delete(str_replace('storage/app/', '', $company->logo));
        try {
            $this->companyRepository->destroy($id);
            $msg = 'Data berhasil dihapus!';
        } catch (Exception $e) {
            $msg = 'Data gagal dihapus!';
        }
        return redirect('transisi/company')->with('msg', $msg);
    }

    /**
     * Return pdf report of specific company
     * @param int $id
     */
    public function print($id)
    {
        $data = $this->companyRepository->print($id);
        $pdf = PDF::loadView('transisi::company.pdf', compact('data'));
        return $pdf->download(date('d-m-Y').'_'.str_replace(' ', '-', $data['company']->name).'.pdf');
    }

    /**
     * Return excel report of all company records
     */
    public function export()
    {
        return (new FastExcel($this->companyRepository->export()))->download('data-company('.date('d-m-Y').').xlsx');
    }

    /**
     * 
     */
    public function import(Request $request)
    {
        $pathFile = $request->csv->storeAs('public', 'importCompany.csv');
        $csv = 'storage/app/'.$pathFile;
        $import=LazyCollection::make(function(){
                    $path = storage_path('app/public/importCompany.csv');
                    $handle = fopen($path, 'r');
                    while($line = fgetcsv($handle)){
                        yield $line;
                    }
                })
                ->chunk(500)
                ->each(function($lines){
                    $list = [];
                    foreach ($lines as $line) {
                        if (isset($line)){
                            $line = explode(';',$line[0]);
                            $list[]=[
                                'name'=>$line[1],
                                'email'=>$line[2],
                                'website'=>$line[3],
                                'logo'=>$line[4]
                            ];
                        }
                    }
                    try {
                        $this->companyRepository->import($list);
                        $this->msg = 'Data berhasil ditambahkan!';
                    } catch (Exception $e) {
                        $this->msg = 'Data gagal ditambahkan!';
                    }
                });
        Storage::delete('public/importCompany.csv');
        return redirect('transisi/company')->with('msg', $this->msg);
    }
}