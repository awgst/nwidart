<?php

namespace Modules\Transisi\Http\Controllers\API;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController;
use Exception;
use Modules\Transisi\Entities\Employee;
use Modules\Transisi\Http\Requests\EmployeeStoreRequest;
use Modules\Transisi\Http\Requests\EmployeeUpdateRequest;
use Modules\Transisi\Repositories\EmployeeRepository;

class EmployeeController extends BaseController
{
    function __construct(EmployeeRepository $employeeRepository)
    {
        $this->employeeRepository = $employeeRepository;
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $employee = $this->employeeRepository->all();
        return $this->responseOk($employee);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(EmployeeStoreRequest $request)
    {
        $validated = $request->validated();
        try {
            return $this->responseOk($this->employeeRepository->store($validated));          
        } catch (Exception $e) {
            throw new $this->responseError('Update gagal!', 422, $e->getMessage());
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        $employee = $this->employeeRepository->find($id);
        return $this->responseOk($employee);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(EmployeeUpdateRequest $request, $id)
    {
        $validated = $request->validated();
        try {
            if ($this->employeeRepository->find($id)!=null) {
                return $this->responseOk($this->employeeRepository->update($validated, $id));
            } else {
                throw new Exception("Update on null", 1);
            }          
        } catch (Exception $e) {
            return $this->responseError('Update gagal!', 422, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        try {
            if ($this->employeeRepository->find($id)!=null) {
                return $this->responseOk($this->employeeRepository->destroy($id));          
            } else {
                throw new Exception("Delete on null", 1);
            }
        } catch (Exception $e) {
            return $this->responseError('Delete gagal!', 422, $e->getMessage());
        }
    }
}
