<?php

namespace Modules\Transisi\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use Exception;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Transisi\Entities\Company;
use Modules\Transisi\Http\Requests\CompanyStoreRequest;
use Modules\Transisi\Http\Requests\CompanyUpdateRequest;
use Modules\Transisi\Repositories\CompanyRepository;

class CompanyController extends BaseController
{
    function __construct(CompanyRepository $companyRepository)
    {
        $this->companyRepository = $companyRepository;
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $company = $this->companyRepository->all();
        return $this->responseOk($company);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(CompanyStoreRequest $request)
    {
        $validated = $request->validated();
        try {
            return $this->responseOk($this->companyRepository->store($validated));          
        } catch (Exception $e) {
            return $this->responseError('Insert gagal!', 422, $e->getMessage());
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        $company = $this->companyRepository->find($id)->load('employees');
        return $this->responseOk($company);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(CompanyUpdateRequest $request, $id)
    {
        $validated = $request->validated();
        try {
            return $this->responseOk($this->companyRepository->update($validated, $id));          
        } catch (Exception $e) {
            return $this->responseError('Update gagal!', 422, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
        try {
            return $this->responseOk($this->companyRepository->destroy($id));          
        } catch (Exception $e) {
            return $this->responseError('Delete gagal!', 422, $e->getMessage());
        }
    }
}
