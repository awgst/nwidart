<?php

namespace Modules\Transisi\Http\Controllers;

use Exception;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Rap2hpoutre\FastExcel\FastExcel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\LazyCollection;
use Modules\Transisi\Entities\Employee;
use Modules\Transisi\Http\Requests\EmployeeStoreRequest;
use Modules\Transisi\Http\Requests\EmployeeUpdateRequest;
use Modules\Transisi\Repositories\EmployeeRepository;

class EmployeeController extends Controller
{
    function __construct(EmployeeRepository $employeeRepository)
    {
        $this->employeeRepository = $employeeRepository;
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $employees = $this->employeeRepository->paginate(5);
        if(request()->ajax()){
            return view('transisi::snippets.employee.pagination', compact('employees'))->render();
        }
        return view('transisi::employee.index', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('transisi::employee.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(EmployeeStoreRequest $request)
    {
        //
        $validated = $request->validated();
        try {
            $this->employeeRepository->store($validated);
            $msg = 'Data berhasil ditambahkan!';
        } catch (Exception $e) {
            $msg = 'Data gagal ditambahkan!';
        }
        return redirect('transisi/employee')->with('msg', $msg);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        $employee = $this->employeeRepository->find($id);
        return view('transisi::employee.show', compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $employee = Employee::find($id);
        return view('transisi::employee.edit', compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(EmployeeUpdateRequest $request, $id)
    {
        //
        $validated = $request->validated();
        try {
            $this->employeeRepository->update($validated, $id);
            $msg = 'Data berhasil diubah!';
        } catch (Exception $e) {
            dd($e);
            $msg = 'Data gagal diubah!';
        }
        return redirect('transisi/employee')->with('msg', $msg);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
        try {
            $this->employeeRepository->destroy($id);
            $msg = 'Data berhasil dihapus!';
        } catch (Exception $e) {
            $msg = 'Data gagal dihapus!';
        }
        return redirect('transisi/employee')->with('msg', $msg);
    }

    /**
     * Return excel report of all company records
     */
    public function export()
    {
        return (new FastExcel($this->employeeRepository->export()))->download('data-employee('.date('d-m-Y').').xlsx');
    }

    /**
     * 
     */
    public function import(Request $request)
    {
        $pathFile = $request->csv->storeAs('public', 'importEmployee.csv');
        $csv = 'storage/app/'.$pathFile;
        $import=LazyCollection::make(function(){
                    $path = storage_path('app/public/importEmployee.csv');
                    $handle = fopen($path, 'r');
                    while($line = fgetcsv($handle)){
                        yield $line;
                    }
                })
                ->chunk(500)
                ->each(function($lines){
                    $list = [];
                    foreach ($lines as $line) {
                        if (isset($line)){
                            $line = explode(';',$line[0]);
                            // dd($line);
                            $list[]=[
                                'name'=>$line[1],
                                'email'=>$line[2],
                                'company_id'=>$line[3]
                            ];
                        }
                    }
                    try {
                        $this->employeeRepository->import($list);
                        $this->msg = 'Data berhasil ditambahkan!';
                    } catch (Exception $e) {
                        $this->msg = 'Data gagal ditambahkan!';
                    }
                });
        Storage::delete('public/importEmployee.csv');
        return redirect('transisi/employee')->with('msg', $this->msg);
    }
    
}
