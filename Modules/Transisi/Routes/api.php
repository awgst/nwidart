<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function () {
    Route::resource('company', API\CompanyController::class);
    Route::resource('employee', API\EmployeeController::class);
    // Route::prefix('employee')->group(function(){
    //     Route::get('/', 'API\CompanyController@index');
    //     Route::post('/', 'API\CompanyController@store');
    //     Route::get('/{id}', 'API\CompanyController@show');
    //     Route::put('/{id}', 'API\CompanyController@update');
    //     Route::delete('/{id}', 'API\CompanyController@destroy');
    // });
});