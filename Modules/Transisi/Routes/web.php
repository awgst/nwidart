<?php
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth'])->group(function () {
    Route::prefix('transisi')->group(function() {
        Route::get('company/export', 'CompanyController@export')->name('company.export');
        Route::get('employee/export', 'EmployeeController@export')->name('employee.export');
        Route::resource('company', CompanyController::class);
        Route::resource('employee', EmployeeController::class);
        Route::post('company/import', 'CompanyController@import')->name('company.import');
        Route::get('company/{id}/print', 'CompanyController@print')->name('company.print');
        Route::post('employee/import', 'EmployeeController@import')->name('employee.import');
    });
});
