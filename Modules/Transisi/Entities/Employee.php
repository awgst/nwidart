<?php

namespace Modules\Transisi\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Employee extends Model
{
    use HasFactory;

    protected $guarded = ['id'];
    
    public function company(){
        return $this->belongsTo(Company::class);
    }
}
