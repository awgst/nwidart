<?php

namespace Modules\Transisi\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Transisi\Entities\Employee;

class TransisiDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(CompanyTableSeeder::class);
        Employee::insert([
            'company_id'=>1,
            'name'=>'karyanto',
            'email'=>'karyanto@gmail.com'
        ]);
        Employee::insert([
            'company_id'=>2,
            'name'=>'karyanti',
            'email'=>'karyanti@gmail.com'
        ]);
        Employee::insert([
            'company_id'=>1,
            'name'=>'karyante',
            'email'=>'karyante@gmail.com'
        ]);
        Employee::insert([
            'company_id'=>1,
            'name'=>'karyanta',
            'email'=>'karyanta@gmail.com'
        ]);
    }
}
