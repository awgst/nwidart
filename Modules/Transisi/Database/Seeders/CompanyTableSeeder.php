<?php

namespace Modules\Transisi\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Transisi\Entities\Company;
use Modules\Transisi\Entities\Employee;

class CompanyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // Create company with random employee count
        // $companies = Company::factory(10)
        //                     ->has(Employee::factory()->count(mt_rand(2,10)))
        //                     ->create();
        Company::insert([
            'name'=>'PT. Test',
            'email'=>'test@gmail.com',
            'logo'=>'test',
            'website'=>'test.com'
        ]);
        Company::insert([
            'name'=>'PT. Test2',
            'email'=>'test2@gmail.com',
            'logo'=>'test2',
            'website'=>'test2.com'
        ]);
        Company::insert([
            'name'=>'PT. Test3',
            'email'=>'test3@gmail.com',
            'logo'=>'test3',
            'website'=>'test3.com'
        ]);
        Company::insert([
            'name'=>'PT. Test4',
            'email'=>'test4@gmail.com',
            'logo'=>'test4',
            'website'=>'test4.com'
        ]);
        Company::insert([
            'name'=>'PT. Test5',
            'email'=>'test5@gmail.com',
            'logo'=>'test5',
            'website'=>'test5.com'
        ]);
        Company::insert([
            'name'=>'PT. Test6',
            'email'=>'test6@gmail.com',
            'logo'=>'test6',
            'website'=>'test7.com'
        ]);
        Company::insert([
            'name'=>'PT. Test7',
            'email'=>'test7@gmail.com',
            'logo'=>'test7',
            'website'=>'test7.com'
        ]);
        Company::insert([
            'name'=>'PT. Test8',
            'email'=>'test8@gmail.com',
            'logo'=>'test8',
            'website'=>'test8.com'
        ]);
    }
}
