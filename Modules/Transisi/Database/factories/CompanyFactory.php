<?php
namespace Modules\Transisi\Database\factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Modules\Transisi\Entities\Company;

class CompanyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Company::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}

