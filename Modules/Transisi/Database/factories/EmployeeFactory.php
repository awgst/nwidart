<?php
namespace Modules\Transisi\Database\factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class EmployeeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = \Modules\Transisi\Entities\Employee::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            // Faker
            'name' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'company_id' => 1
        ];
    }
}

